<?php

namespace App\Http\Controllers;

use App\Event;
use App\EventSchedule;
use Illuminate\Http\Request;
use Symfony\Component\Console\Input\Input;

class EventController extends Controller
{
    public function getEvents(Request $req)
    {
        $startDate = $req->query('start');
        $endDate = $req->query('end');
        $query = Event::query()->select()->with('schedules');

        if ($startDate) {
            $query = $query->where('end_date', '>=', $startDate);
        }

        if ($endDate) {
            $query = $query->where('start_date', '<=', $endDate);
        }

        return $query->get()->toArray();
    }

    public function getById($id)
    {
        return Event::query()
            ->where('id', $id)
            ->with('schedules')
            ->first();
    }

    public function postEvent(Request $req)
    {
        $eventId = $req->json('id');
        $event = $eventId ? $this->getById($eventId) : new Event();
        $event->fill($req->json()->all());
        $event->save();

        $schedules = $req->json('schedules');
        if ($schedules) {
            $eventScheds = array_map(
                function ($sched) {
                    return new EventSchedule($sched);
                },
                $schedules);

            $event->schedules()->delete();
            $event->schedules()->saveMany($eventScheds);
        } else {
            $event->schedules()->delete();
        }

        return $this->getById($event->id);
    }
}
