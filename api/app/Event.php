<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Event
 * @package App
 * @property int $id
 * @property \DateTime $created_at
 * @property \DateTime $updated_at
 * @property string $name
 * @property \DateTime $start_date
 * @property \DateTime $end_date
 */
class Event extends Model
{
    protected $fillable = ['name', 'start_date', 'end_date'];

    public function schedules() {
        return $this->hasMany(EventSchedule::class);
    }
}
