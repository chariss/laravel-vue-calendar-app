<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class EventSchedule
 * @package App
 * @property int $id
 * @property \DateTime $created_at
 * @property \DateTime $updated_at
 * @property int day_of_week 1 - monday ... 7 - sunday
 */
class EventSchedule extends Model
{
    protected $fillable = ['day_of_week'];
}
