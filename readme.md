# Laravel + Vue Calendar App

## Laravel Dev Server

In api/laradock directory run `./start-docker-server.sh`. The laravel API app listens at http://localhost:8081 and PHPMyadmin at http://localhost:8082.

The script file `start-docker-server.sh` will do a `docker-compose exec` into the workspace service of laradock. Once inside the workspace shell, you can execute artisan commands such as `php artisan migrate` to initialize the db.


## Vue Dev Server

In web/ directory run `npm run serve`. The Vue server listens at http://localhost:8080.


---
This repo is for demo purposes only of my job application. Will make this private after verification on the reviewer's side.