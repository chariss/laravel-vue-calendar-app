import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const apiUrl = 'http://localhost:8081/api/events'

const post = (url = '', data = {}) => {
  // Default options are marked with *
  return fetch(url,
    {
      method: 'POST',
      cache: 'no-cache',
      mode: 'no-cors',
      headers: {
        'Content-Type': 'application/json'
      },
      redirect: 'follow',
      referrer: 'no-referrer',
      body: JSON.stringify(data)
    })
}

export default new Vuex.Store({
  state: {
    filter: {
      start: undefined,
      end: undefined
    },
    event: undefined,
    events: []
  },
  mutations: {
    setEvents (state, events) {
      Vue.set(state, 'events', events)
    },
    setSelected (state, event) {
      Vue.set(state, 'event', event)
    },
    setFilter (state, filter) {
      Vue.set(state, 'filter', filter)
    }
  },
  actions: {
    save (store, event) {
      const filter = store.state.filter || {}
      post(apiUrl, event)
        .then(res => store.dispatch('getEvents', filter))
    },
    getEvents (store, { start, end }) {
      store.commit('setFilter', { start, end })

      fetch(`${apiUrl}?start=${start}&end=${end}`)
        .then(res => res.json())
        .then(events => store.commit('setEvents', events))
    }
  },
  modules: {
  }
})
