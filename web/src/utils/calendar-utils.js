const eventColors = [
  '#2a9a42',
  '#8c3f2b',
  '#2196F3',
  '#3F51B5',
  '#1565C0',
  '#009688',
  '#004D40',
  '#1B5E20',
  '#E65100',
  '#2948b0'
]

const daysShortName = [
  'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'
]

/**
 * Extracts the API's event object schedule as days array
 *
 * @param {Object} event  API's event object
 * @returns {number[]}
 */
export const getEventDaysArray = (event) => event.schedules
  .map(sched => sched.day_of_week > 6 ? 0 : sched.day_of_week) // Note: Backend: 1 - mon ... 7 - sunday (JS Date: 0 - 6)
  .sort()

/**
 * Converts the days in a week array into its corresponding names
 *
 * @param {number[]} days
 * @return {string[]}
 */
export const getDaysName = (days) => days.map(d => daysShortName[d])

/**
 * Converts the api event data into calendar event object
 *
 * @param {Object} event  api event object
 * @param {Date} start    calendar start date
 * @param {Date} end      calendar end date
 */
export function apiDataToEvents (event, start, end) {
  const evtStart = new Date(event.start_date)
  const evtEnd = new Date(event.end_date)

  const rangeStart = start > evtStart ? start : evtStart
  const rangeEnd = end < evtStart ? end : evtEnd
  const evtColor = eventColors[event.id % eventColors.length]
  const evtDays = getEventDaysArray(event)

  const calEvents = []
  let date = rangeStart
  while (date <= rangeEnd) {
    if (evtDays.indexOf(date.getDay()) >= 0) {
      const calEvtDate = formatDate(date)
      calEvents.push({
        name: event.name,
        details: `
         Date: ${formatDate(new Date(event.start_date))} - ${formatDate(new Date(event.end_date))}<br>
         Every: ${getDaysName(evtDays).join(', ')}`,
        start: calEvtDate,
        end: calEvtDate,
        color: evtColor,
        data: event
      })
    }

    date = new Date(date.getTime())
    date.setDate(date.getDate() + 1)
  }

  return calEvents
}

/**
 * Formats the date object to 'yyyy-mm-dd'
 *
 * @param {Date} date
 * @returns {string}
 */
export function formatDate (date) {
  const year = date.getFullYear()
  const month = padLeft(date.getMonth() + 1, '00')
  const day = padLeft(date.getDate(), '00')

  return `${year}-${month}-${day}`
}

/**
 * Adds a left padding to a value
 *
 * @param {string|number} value
 * @param {string} placeholder
 * @returns {string}
 */
export function padLeft (value, placeholder) {
  return (placeholder + value).substr(-placeholder.length)
}

/**
 * Converts the vuetify calendar date object to JS Date
 *
 * @param {Object} dateObj
 * @returns {Date}
 */
export function calDateObjToJsDate (dateObj) {
  return new Date(dateObj.year, dateObj.month - 1, dateObj.day)
}

export function nth (d) {
  return d > 3 && d < 21
    ? 'th'
    : ['th', 'st', 'nd', 'rd', 'th', 'th', 'th', 'th', 'th', 'th'][d % 10]
}
